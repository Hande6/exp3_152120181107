/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include "Square.h"

Square::Square(double a) {
	setA(a);
	setB(a);
}

Square::~Square() {
}

void Square::setA(double a) {
	Square::a = a;
	Square::b = b;
}

void Square::setB(double b) {
	Square::b = b;
	Square::a = a;
}

double Square::calculateCircumference() {
	return (Square::a + Square::b) * 2;
}

double Square::calculateArea() {
	return Square:: a *Square:: b;
}
